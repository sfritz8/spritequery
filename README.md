To access the project through Unity: 
First clone the git project from this link:
https://git.cs.vt.edu/sfritz8/spritequery

Into an accessible folder on your computer.
Then, install Unity Hub from this link:
https://unity3d.com/get-unity/download

And use the Unity Editor ver 2020.3.30f1
Once in the Unity Hub Click "Open" and "Add Project From Disk". Then locate the folder that you cloned to on your computer to add Spritequery to your hub. 

To access the build of the project use this link: 
https://drive.google.com/drive/folders/1e-cFIoSRMo4bIDHhEyFwUGO4Xwmf-jnG?usp=sharing

Download the FinalBuild.zip file and extract it. 
Launch Spritequery.exe 

References for models and materials used in Spritequery can be found listed here: 
https://docs.google.com/document/d/15JHpLt8qDVinJoj1qAjDIXODR7j_FLwK-mdzOCm8FMk/edit?usp=sharing
