using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CavePuzzle : MonoBehaviour
{
    int numCollected = 0;
    public GameObject door;
    public void pickUpPackage()
    {
        numCollected++;
        if (numCollected >= 5)
        {
            opendoor();
        }
    }

    public void opendoor()
    {
        door.SetActive(true);
        Debug.Log("door open");
    }
}
