using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CavePuzzleTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private bool pickedUp = false;

    private GameObject target;
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.E))
        {
            if (!pickedUp && target != null)
            {
                Transform child = target.transform.Find("FX_Fire");
                child.gameObject.SetActive(true);
                pickedUp = true;
                Debug.Log("test");
                FindObjectOfType<CavePuzzle>().pickUpPackage();
            }
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "FirstPersonController")
        {
            Debug.Log("Collided");
            target = this.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("exit Collision");
    }
}
