using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// based off of this tutorial: https://www.youtube.com/watch?v=_nRzoTzeyxU&ab_channel=Brackeys
[System.Serializable]
public class Dialogue
{

    [TextArea(1, 10)]
    public string[] sentences;
    public string name;
}
