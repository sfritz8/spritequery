using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// based off of this tutorial: https://www.youtube.com/watch?v=_nRzoTzeyxU&ab_channel=Brackeys
public class CountryDialogue : MonoBehaviour
{
    public string level1;
    public string level2;
    public Queue<string> sentences;
    public Text dialogueText;
    public Text nameText;

    public InputField inputField;
    public bool textEntry;

    public Animator animator;
    public AudioSource chatter;
    public bool isLock = false;


    
    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>();
    }

    void Update() {
        
        if (textEntry) {
            if (Input.GetMouseButtonDown(0)) {
                inputField.ActivateInputField();
            }
            if (Input.GetKeyDown(KeyCode.Return)) {
                FindObjectOfType<DialoguePuzzle>().checkAnswer(new string[] {"1", "4", "9", "5", "2"});
                EndDialogue();
            }
        }
        else {
            if (Input.GetMouseButtonDown(0)) {
                DisplayNextSentence();
            }
        }
    }

    public void StartDialogue(Dialogue dialogue, bool isLocked) {
        if (isLocked) {
            isLock = true;
        }
        animator.SetBool("IsOpen", true);
        Debug.Log("Starting conversation");
        nameText.text = dialogue.name;
        sentences.Clear();

        foreach (string sentence in dialogue.sentences) {
            sentences.Enqueue(sentence);
        }
        
        DisplayNextSentence();
       
    }

    public void DisplayNextSentence() {
        if (sentences.Count == 0) {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();

        if (sentence.Contains(" ?")) {
            textEntry = true;
        }

        if (textEntry) {
            inputField.gameObject.SetActive(true);
            GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>().enabled = false;
        }
        else {
            inputField.gameObject.SetActive(false);
        }

        Debug.Log(sentence);
        StopAllCoroutines();
        StartCoroutine(TypeSenctence(sentence));
        
    }

    IEnumerator TypeSenctence (string sentence) {
        if (!isLock){
            chatter.Play();
        }
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray()) {
            dialogueText.text += letter;
            yield return new WaitForSeconds(0.05f);
        }
        
        if (textEntry) {
            Debug.Log("activating");
            inputField.ActivateInputField();
        }     
        if (!isLock) {
            StartCoroutine(FadeOut(chatter, 1.0f));
        }
    }

    public static IEnumerator FadeOut (AudioSource audioSource, float FadeTime) {
        float startVolume = audioSource.volume;
 
        while (audioSource.volume > 0) {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
 
            yield return null;
        }
 
        audioSource.Stop ();
        audioSource.volume = startVolume;
    }

    public void EndDialogue() {
        if (textEntry) {            
            GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>().enabled = true;
        }
        textEntry = false;
        inputField.text = "";
        if (!isLock) {
            chatter.Stop();
        }
        Debug.Log("End of conversation");
        animator.SetBool("IsOpen", false);
    }
}
