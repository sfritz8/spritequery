using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Text.RegularExpressions;

public class BeachTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public bool room1;
    public bool room2;
    public bool room3;

    private Dictionary<string, string> questions;
    private Dictionary<string, string> representations;

    // based off of this tutorial: https://www.youtube.com/watch?v=_nRzoTzeyxU&ab_channel=Brackeys
    public void TriggerDialogue() {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.name == "FirstPersonController") {
            FindObjectOfType<BeachDialogue>().StartDialogue(dialogue);
        }
    }

    void OnCollisionExit(Collision collision) {
        if (collision.gameObject.name == "FirstPersonController") {
            FindObjectOfType<BeachDialogue>().EndDialogue();
        }
    }

    string getRoomName(string name) {
        switch (name)
        {
            case null: throw new ArgumentNullException(nameof(name));
            case "": throw new ArgumentException($"{nameof(name)} cannot be empty", nameof(name));
            default: return Regex.Replace(name[0].ToString().ToUpper() + name.Substring(1), "([a-z])([A-Z])", "$1 $2");
        }
    }
}
