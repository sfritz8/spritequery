using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Text.RegularExpressions;

public class Trigger : MonoBehaviour
{
    public Dialogue dialogue;
    public bool room1;
    public bool room2;
    public bool room3;
    public bool room4;
    public bool room5;

    private Dictionary<string, string> questions;
    private Dictionary<string, string> representations;

    // based off of this tutorial: https://www.youtube.com/watch?v=_nRzoTzeyxU&ab_channel=Brackeys
    public void TriggerDialogue() {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }

    void OnCollisionEnter(Collision collision) {
        questions = new Dictionary<string, string>();
        representations = new Dictionary<string, string>();
        questions.Add("flowerRoom", "how you deal with anxiety");
        questions.Add("desertRoom", "how you deal with anxiety");
        questions.Add("mountainRoom", "what was your greatest failure");
        questions.Add("beachRoom2", "what was your greatest failure");
        questions.Add("cityRoom", "how you aspire to live your life");
        questions.Add("countryRoom", "how you aspire to live your life");
        questions.Add("skyRoom", "are you happy with your life");
        questions.Add("caveRoom2", "are you happy with your life");
        questions.Add("lavaRoom", "how you respond to criticism");
        questions.Add("iceRoom", "how you respond to criticism");

        representations.Add("flowerRoom", "connection");
        representations.Add("desertRoom", "solitude");
        representations.Add("mountainRoom", "perserverance");
        representations.Add("beachRoom2", "ebb and flow");
        representations.Add("cityRoom", "high goals");
        representations.Add("countryRoom", "dedication");
        representations.Add("skyRoom", "freedom");
        representations.Add("caveRoom2", "frustration");
        representations.Add("lavaRoom", "boldness");
        representations.Add("iceRoom", "peaceful");

        if (room1) {
            
            setDialogue((string) ResponseManager.Instance.rooms[0], (string) ResponseManager.Instance.responses[0]);     
        }
        if (room2) {
            setDialogue((string) ResponseManager.Instance.rooms[1], (string) ResponseManager.Instance.responses[1]);     
        }
        if (room3) {
            setDialogue((string) ResponseManager.Instance.rooms[2], (string) ResponseManager.Instance.responses[2]);     
        }
        if (room4) {
            setDialogue((string) ResponseManager.Instance.rooms[3], (string) ResponseManager.Instance.responses[3]);     
        }
        if (room5) {
            setDialogue((string) ResponseManager.Instance.rooms[4], (string) ResponseManager.Instance.responses[4]);     
        }
        if (collision.gameObject.name == "FirstPersonController") {
            FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        }
    }

    void OnCollisionExit(Collision collision) {
        if (collision.gameObject.name == "FirstPersonController") {
            FindObjectOfType<DialogueManager>().EndDialogue();
        }
    }

    void setDialogue(string room, string response) {
        dialogue.sentences[0] = "I asked " + questions[room] + ", and you answered: " + response;
        dialogue.sentences[1] = "I chose the " + getRoomName(room) + " to represent " + representations[room] + " for you.";
    }

    string getRoomName(string name) {
        if (name.Equals("caveRoom2")) {
            return "Cave Room";
        }
        if (name.Equals("beachRoom2")) {
            return "Beach Room";
        }
        switch (name)
        {
            case null: throw new ArgumentNullException(nameof(name));
            case "": throw new ArgumentException($"{nameof(name)} cannot be empty", nameof(name));
            default: return Regex.Replace(name[0].ToString().ToUpper() + name.Substring(1), "([a-z])([A-Z])", "$1 $2");
        }
    }
}
