using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Text.RegularExpressions;

public class CountryTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public bool room1;
    public bool room2;
    public bool room3;

    private Dictionary<string, string> questions;
    private Dictionary<string, string> representations;

    // based off of this tutorial: https://www.youtube.com/watch?v=_nRzoTzeyxU&ab_channel=Brackeys
    public void TriggerDialogue() {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.name == "FirstPersonController") {
            if (this.gameObject == GameObject.FindGameObjectWithTag("Lock")){
                FindObjectOfType<CountryDialogue>().StartDialogue(dialogue, true);
            }
            else {
                FindObjectOfType<CountryDialogue>().StartDialogue(dialogue, false);
            }
            
        }
    }

    void OnCollisionExit(Collision collision) {
        if (collision.gameObject.name == "FirstPersonController") {
            FindObjectOfType<CountryDialogue>().EndDialogue();
        }
    }
}
