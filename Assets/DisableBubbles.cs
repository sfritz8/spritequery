using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableBubbles : MonoBehaviour
{
    public GameObject[] bubbles;
    public ParticleSystem bubbleParticleSystem;
    // Start is called before the first frame update
    void Start()
    {
        bubbles = GameObject.FindGameObjectsWithTag("Bubbles");
        GameObject bubbleParticleObject = bubbles[0];
        bubbleParticleSystem = bubbleParticleObject.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if(this.transform.position.y >= 7.5)
        {
            bubbleParticleSystem.Stop();
        }
        else if(!bubbleParticleSystem.isPlaying)
        {
            bubbleParticleSystem.Play();
        }
    }
}
