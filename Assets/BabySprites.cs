using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabySprites : MonoBehaviour
{
    public GameObject flowerSprite;
    public GameObject desertSprite;
    public GameObject mountainSprite;
    public GameObject beachSprite;
    public GameObject citySprite;
    public GameObject countrySprite;
    public GameObject skySprite;
    public GameObject caveSprite;
    public GameObject iceSprite;
    public GameObject lavaSprite;

    public bool flower;
    public bool desert;
    public bool mountain;
    public bool beach;
    public bool city;
    public bool country;
    public bool sky;
    public bool cave;
    public bool ice;
    public bool lava;


    void Start()
    {
        if(ResponseManager.Instance.rooms.Count >= 1)
        {
            if (!flower && ResponseManager.Instance.rooms[0].Equals("flowerRoom"))
            {
                flowerSprite.SetActive(true);
            }
            if (!desert && ResponseManager.Instance.rooms[0].Equals("desertRoom"))
            {
                desertSprite.SetActive(true);
            }
        }
        if (ResponseManager.Instance.rooms.Count >= 2)
        {
            if (!mountain && ResponseManager.Instance.rooms[1].Equals("mountainRoom"))
            {
                mountainSprite.SetActive(true);
            }
            if (!beach && ResponseManager.Instance.rooms[1].Equals("beachRoom2"))
            {
                beachSprite.SetActive(true);
            }
        }
        if (ResponseManager.Instance.rooms.Count >= 3)
        {
            if (!city && ResponseManager.Instance.rooms[2].Equals("cityRoom"))
            {
                citySprite.SetActive(true);
            }
            if (!country && ResponseManager.Instance.rooms[2].Equals("countryRoom"))
            {
                countrySprite.SetActive(true);
            }
        }
        if (ResponseManager.Instance.rooms.Count >= 4)
        {
            if (!sky && ResponseManager.Instance.rooms[3].Equals("skyRoom"))
            {
                skySprite.SetActive(true);
            }
            if (!cave && ResponseManager.Instance.rooms[3].Equals("caveRoom2"))
            {
                caveSprite.SetActive(true);
            }
        }
        if (ResponseManager.Instance.rooms.Count >= 5)
        {
            if (!ice && ResponseManager.Instance.rooms[4].Equals("iceRoom"))
            {
                iceSprite.SetActive(true);
            }
            if (!lava && ResponseManager.Instance.rooms[4].Equals("lavaRoom"))
            {
                lavaSprite.SetActive(true);
            }
        }



        
        
        
    }
}
