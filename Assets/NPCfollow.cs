using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// https://www.youtube.com/watch?v=zssU0MZcIx8&ab_channel=JimmyVegas
public class NPCfollow : MonoBehaviour
{
   public GameObject ThePlayer;
   public float TargetDistance;
   public float AllowedDistance = 5f;
   public GameObject TheNPC;
   public float FollowSpeed;
   public RaycastHit Shot;

    void Update()
    {
        transform.LookAt(ThePlayer.transform);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out Shot)) {
            TargetDistance = Shot.distance;
            if (TargetDistance >= AllowedDistance) {
                FollowSpeed = 0.02f;
                transform.position = Vector3.MoveTowards(transform.position, ThePlayer.transform.position, FollowSpeed);
            }
            else {
                FollowSpeed = 0;
            }
        }
    }
}
