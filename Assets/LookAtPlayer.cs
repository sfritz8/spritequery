using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    public Transform target;
    public int x;
    public int y;
    public int z;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);
        transform.Rotate(x, y, z);
    }
}
