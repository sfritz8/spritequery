using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PuzzleManager : MonoBehaviour
{
    int numCollected = 0;
    public GameObject door;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void collectMiniSprite() {
        numCollected++;
        Debug.Log("numCollected = " + numCollected);
        if (numCollected >= 3) {
            opendoor();
            return;
        }
    }

    public void opendoor() {
        door.SetActive(true);
        Debug.Log("door open");
    }
}
