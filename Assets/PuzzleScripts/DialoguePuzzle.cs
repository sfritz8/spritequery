using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialoguePuzzle : MonoBehaviour
{
    public InputField inputField;
    public GameObject door;

    public void checkAnswer(string[] codenums) {
        string code = inputField.text;
        if(code.Contains(codenums[0]) && code.Contains(codenums[1]) && code.Contains(codenums[2]) && code.Contains(codenums[3]) && code.Contains(codenums[4]) && (code.Length == 5))
        {
            opendoor();
        }
    }

    public void opendoor() {
        door.SetActive(true);
        Debug.Log("door open");
    }
}
