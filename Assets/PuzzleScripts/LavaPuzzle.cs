using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LavaPuzzle : MonoBehaviour
{
    float targetTime = 45f;
    bool start = false;
    public Text timerText;

    public GameObject door;

    void Update() {
        if (start) {
            targetTime -= Time.deltaTime;
            TimeSpan t = TimeSpan.FromSeconds(targetTime);
            timerText.text = t.ToString(@"mm\:ss\:fff");
            if (targetTime <= 0.0f) {
                timerEndedFail();
            }
        }
    }

    public void timerEndedFail() {
        timerText.text = "TIME RAN OUT!";
        FindObjectOfType<FirstPersonController>().gameObject.transform.position = new Vector3(1, 3, 7.5f);
        start = false;
        targetTime = 45f;
        StartCoroutine(hideTimer());
    }

    public void timerEndedSuccess() {
        start = false;
        timerText.enabled = false;
        targetTime = 45f;
        opendoor();
    }
    
    public IEnumerator hideTimer() {
        yield return new WaitForSeconds(3);
        if (!start) {
            timerText.enabled = false;
        }
    }
    void OnCollisionEnter(Collision other) {
        if (other.gameObject.name == "sprite") {
            start = false;
            timerText.enabled = false;
            targetTime = 45f;
        }
        if (other.gameObject.name == "sprite2") {  
            Debug.Log("Collided");
            timerEndedSuccess();
        }
    }

    void OnCollisionExit(Collision other) {
        if (other.gameObject.name == "sprite") {
            Debug.Log("exit Collision");
            start = true;
            Debug.Log("enable timer");
            timerText.enabled = true;
        }
    }

    public void opendoor() {
        door.SetActive(true);
        Debug.Log("door open");
    }
}
