using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainPuzzleTrigger : MonoBehaviour
{
    private bool pickedUp = false;

    private GameObject target;
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.E)) {
            if (!pickedUp && target != null) {
                target.SetActive(false);
                pickedUp = true;
                Debug.Log("test");
                
                FindObjectOfType<MountainPuzzle>().pickUpGemstone();
            }
        }
    }
    void OnCollisionEnter(Collision other) {
        if (other.gameObject.name == "FirstPersonController") {  
            Debug.Log("Collided");
            target = this.gameObject;
        }
    }

    void OnCollisionExit(Collision other) {
        Debug.Log("exit Collision");
        
    }
}
