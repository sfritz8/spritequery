using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PuzzleTrigger : MonoBehaviour
{

    public NavMeshAgent miniSprite;
    public Transform player;

    bool follow = false;
    void OnCollisionEnter(Collision other) {
        if (!follow && other.gameObject.name == "FirstPersonController") {
            Debug.Log("collision started");
            FindObjectOfType<PuzzleManager>().collectMiniSprite();
            follow = true;
            if (this.gameObject.GetComponent<Collider>() != null){
                Destroy(this.gameObject.GetComponent<Collider>());
            }
        }
    }

    void Update() {
        if (follow) {
            miniSprite.SetDestination(player.position);
        }
    }
}
