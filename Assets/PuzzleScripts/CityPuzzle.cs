using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityPuzzle : MonoBehaviour
{
    int numCollected = 0;
    public GameObject door;
    public void pickUpPackage() {
        numCollected++;
        if (numCollected >= 9) {
            opendoor();
        }
    }

    public void opendoor() {
        door.SetActive(true);
        Debug.Log("door open");
    }
}
