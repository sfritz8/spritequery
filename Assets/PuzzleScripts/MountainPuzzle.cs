using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainPuzzle : MonoBehaviour
{
    int numCollected = 0;
    public GameObject door;
    public void pickUpGemstone() {
        numCollected++;
        if (numCollected >= 5) {
            opendoor();
        }
    }

    public void opendoor() {
        door.SetActive(true);
        Debug.Log("door open");
    }
}
