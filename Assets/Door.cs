using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Door : MonoBehaviour
{
    public GameObject door;
    public string level;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other) {
        Debug.Log("Left scene");
        SceneManager.LoadScene(level);
        door.SetActive(false);
    }
}
