using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// based off this guide: https://learn.unity.com/tutorial/implement-data-persistence-between-scenes#60b74182edbc2a54f13d5f2e
public class ResponseManager : MonoBehaviour
{
    public static ResponseManager Instance;
    public ArrayList rooms;
    public ArrayList responses;
    // Start is called before the first frame update
    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void addResponse(string room, string response) {
        if (rooms == null) {
            rooms = new ArrayList();
            responses = new ArrayList();
        }

        rooms.Add(room);
        responses.Add(response);
    }
}
