using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyRoomPuzzleTrigger : MonoBehaviour
{

    private GameObject target;
    void Update()
    {
        if (target != null)
        {
            target.SetActive(false);
            Debug.Log("test");
            FindObjectOfType<SkyRoomPuzzle>().pickUpPackage();
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Collided");
            target = this.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("exit Collision");
    }
}
