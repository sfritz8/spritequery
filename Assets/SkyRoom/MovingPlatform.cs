using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    //https://blog.devgenius.io/moving-platforms-in-unity-4d7299b2d013 Code Referenced from this article
    public Transform targetA, targetB;
    public float speed = 1.0f;
    public bool start = false;
    private Vector3 targetAPosition, targetBPosition;
    private Vector3 targetPosition, position;
    // Start is called before the first frame update
    void Start()
    {
        //Initial conditions 
        targetAPosition = targetA.position;
        targetBPosition = targetPosition = targetB.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(start)
        {
            position = transform.position;

            transform.position = Vector3.MoveTowards(position, targetPosition, speed * Time.deltaTime);
            if (position == targetAPosition)
            {
                targetPosition = targetBPosition;
            }
            else if (position == targetBPosition)
            {
                targetPosition = targetAPosition;
            }
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        //If the player collides with the platform, set it's parent to the platform
        if (other.tag == "Player")
        {
            Debug.Log("True");
            start = true;
            other.gameObject.transform.parent = this.gameObject.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.transform.parent = null;
        }
    }
}

